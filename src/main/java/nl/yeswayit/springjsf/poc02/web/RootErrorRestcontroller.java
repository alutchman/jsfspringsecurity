package nl.yeswayit.springjsf.poc02.web;


import nl.yeswayit.springjsf.poc02.data.messages.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;


@Controller
public class RootErrorRestcontroller implements ErrorController {
    private static final String PATH = "/error";
    private boolean debug = false;

    @Autowired
    private ErrorAttributes errorAttributes;

    @RequestMapping(value = "/error")
    public ModelAndView error(WebRequest webRequest) {
        Map<String, Object> errorInfo = errorAttributes.getErrorAttributes(webRequest, debug);
        final ErrorResponse errorResponse = new ErrorResponse(errorInfo);

        Map<String, Object> datamap = new HashMap<String, Object>();
        datamap.put("errorInfo", errorResponse);
        return new ModelAndView("error",datamap);
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}
