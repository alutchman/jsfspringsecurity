package nl.yeswayit.springjsf.poc02.web;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Controller
public class BaseController  {

    @RequestMapping(value="/")
    public ModelAndView defaultmap(HttpServletRequest request, HttpServletResponse response){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            return new ModelAndView("test");
        } else {
            return new ModelAndView("login");
        }

    }


    @RequestMapping(value="login.html")
    public ModelAndView loginGetA(HttpServletRequest request, HttpServletResponse response){
        Map<String, Object> datamap = new HashMap<String, Object>();

        return new ModelAndView("login",datamap);
    }


    @RequestMapping(value="test.html")
    public ModelAndView loginGetB(HttpServletRequest request, HttpServletResponse response){
        Map<String, Object> datamap = new HashMap<String, Object>();


        return new ModelAndView("test",datamap);
    }

    @RequestMapping(value="admin.html")
    public ModelAndView loginGetC(HttpServletRequest request, HttpServletResponse response){
        Map<String, Object> datamap = new HashMap<String, Object>();

        return new ModelAndView("admin",datamap);
    }

    @RequestMapping(value="denied.html")
    public ModelAndView loginGetD(HttpServletRequest request, HttpServletResponse response){
        Map<String, Object> datamap = new HashMap<String, Object>();

        return new ModelAndView("denied",datamap);
    }


}
