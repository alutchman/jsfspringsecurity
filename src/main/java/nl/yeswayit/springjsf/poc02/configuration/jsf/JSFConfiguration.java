package nl.yeswayit.springjsf.poc02.configuration.jsf;

import com.sun.faces.config.ConfigureListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.boot.context.embedded.ServletContextInitializer;
import org.springframework.boot.context.embedded.ServletListenerRegistrationBean;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import javax.faces.webapp.FacesServlet;
import javax.servlet.DispatcherType;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.EnumSet;
import java.util.HashMap;


/**
 * Created by anonymous on 1/31/16.
 */
@Configuration
public class JSFConfiguration {
    /**
     * Configuración de ViewScope
     *
     * @return ViewScope
     */
    @Bean
    public static ViewScope viewScope() {
        return new ViewScope();
    }

    /**
     * Allows the use of @Scope("view") on Spring @Component, @Service and @Controller
     * beans
     */
    @Autowired
    @Bean
    public static CustomScopeConfigurer scopeConfigurer(ViewScope viewScope) {
        CustomScopeConfigurer configurer = new CustomScopeConfigurer();
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("view", viewScope);
        configurer.setScopes(hashMap);
        return configurer;
    }

    /*
     * Below sets up the Faces Servlet for Spring Boot
     */
    @Bean
    public FacesServlet facesServlet() {
        return new FacesServlet();
    }

    /**
     * Configuración de JSF
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean facesServletRegistration() {
        ServletRegistrationBean registration = new ServletRegistrationBean(
                facesServlet(), "*.xhtml");
        registration.setName("FacesServlet");
        registration.setLoadOnStartup(1);
        return registration;
    }

    /**
     * Configuración de JSF
     *
     * @return
     */
    @Bean
    public ServletListenerRegistrationBean<ConfigureListener> jsfConfigureListener() {
        return new ServletListenerRegistrationBean<ConfigureListener>(
                new ConfigureListener());
    }


    @Bean
    public ViewResolver getViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/views/");
        resolver.setSuffix(".xhtml");

        return resolver;
    }


    /**
     * Configuración de servlet startup de JSF
     *
     * @return
     */
    @Bean
    public ServletContextInitializer initializer() {
        return new ServletContextInitializer() {

            @Override
            public void onStartup(ServletContext servletContext)
                    throws ServletException {

                ServletRegistration.Dynamic facesServlet = servletContext.addServlet("Faces Servlet", FacesServlet.class);

                facesServlet.addMapping("*.jsf","*.xhtml");
                facesServlet.setLoadOnStartup(1);


                //Theme selection
                servletContext.setInitParameter("primefaces.THEME",
                        "bootstrap");
                servletContext.setInitParameter("javax.faces.INTERPRET_EMPTY_STRING_SUBMITTED_VALUES_AS_NULL","true");

                //Configuración de taglibs con Spring Security
                servletContext.setInitParameter(
                        "com.sun.faces.forceLoadConfiguration", "true");
                EnumSet<DispatcherType> tiposDispatcher = EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD);
                servletContext.addFilter("securityFilter",
                        new DelegatingFilterProxy("springSecurityFilterChain")).addMappingForUrlPatterns(tiposDispatcher, false, "/*");
            }
        };
    }
}
