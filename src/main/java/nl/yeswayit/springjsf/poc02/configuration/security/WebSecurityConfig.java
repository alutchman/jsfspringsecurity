package nl.yeswayit.springjsf.poc02.configuration.security;

import nl.yeswayit.springjsf.poc02.service.SystemUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * Config Spring Security
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
    @Autowired
    SystemUserDetailsService systemUserRepository;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //Para visualización de Consola de H2
        http.headers().frameOptions().disable();

        //CSRF es manejado por JSF
        http.csrf().disable();
        http.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .invalidateHttpSession(true)
                .logoutSuccessUrl("/login.html");

        http.authorizeRequests()
                .antMatchers("/error").permitAll()
                .antMatchers("/login.html").permitAll()
                .antMatchers("/test.html").hasAnyRole("USER","ADMIN")
                .antMatchers("/admin.html").hasRole("ADMIN")
                .and().formLogin().loginPage("/login.html")
                .loginProcessingUrl("/login").defaultSuccessUrl("/test.html").and()
                .exceptionHandling().accessDeniedPage("/denied.html");
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(systemUserRepository).passwordEncoder(passwordEncoder());
    }


    @Bean
    public PasswordEncoder passwordEncoder(){
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }

}